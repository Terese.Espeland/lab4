package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        // TODO Auto-generated method stub
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        // TODO Auto-generated method stub
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

    @Override
    public void step() {
        // TODO Auto-generated method stub
        IGrid nextGeneration = currentGeneration.copy();
		for (int i = 0; i < numberOfRows(); i++) {
			for (int j = 0; j < numberOfColumns(); j++) {
				nextGeneration.set(i, j, getNextCell(i, j));
			}
		}
		this.currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        // TODO Auto-generated method stub
        int aliveNeighbours = countNeighbors(row, col, CellState.ALIVE);
		if (getCellState(row, col).equals(CellState.ALIVE)) {
            return CellState.DYING;
        } else if (getCellState(row, col).equals(CellState.DYING)) {
            return CellState.DEAD;
        } else {
            if (aliveNeighbours == 2) {
                return CellState.ALIVE;
            } else {
                return CellState.DEAD;
            }
        }
    }

    @Override
    public int numberOfRows() {
        // TODO Auto-generated method stub
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        // TODO Auto-generated method stub
        return currentGeneration.numColumns();
    }

    private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int numNeighbours = 0;
		
		for (int i = row-1; i < row+2; i++) {
			for (int j = col-1; j < col+2; j++) {
				if (i == row && j == col) {
					continue;
				}
				if (i < numberOfRows() && j < numberOfColumns() && i >= 0 && j >= 0 && (getCellState(i, j).equals(state))) {
					numNeighbours += 1;
				}
			}
		}
		return numNeighbours;
    }

    @Override
    public IGrid getGrid() {
        // TODO Auto-generated method stub
        return currentGeneration;
    }
    
}
